<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $hasher = app()->make('hash');

        $email = $request->input('email');
        $password = $request->input('password');

        $login = User::where('email', $email)->first();
        if(!$login){
            $res['success'] = false;
            $res['message'] = "Your email incorrect";
        } else {
            if($hasher->check($password, $login->password)){
                $api_token = sha1(time());
                $create_token = User::where('id', $login->id)->update(['api_token' => $api_token]);
                if($create_token){
                    $res['success'] = true;
                    $res['message'] = $login;
                    $res['api_token'] = $api_token;
                } else {
                    $res['success'] = false;
                    $res['message'] = "Somthing wrong";
                }
            } else {
                $res['success'] = false;
                $res['message'] = "Your password incorrect";
            }
        }
        return response($res);
    }
    //
}
