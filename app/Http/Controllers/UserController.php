<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request)
    {
        $hasher = app()->make('hash');

        $username = $request->input('username');
        $email = $request->input('email');
        $password = $hasher->make($request->input('password'));

        $register = User::create([
            'username' => $username,
            'email' => $email,
            'password' => $password
        ]);

        if($register){
            $res['success'] = true;
            $res['message'] = 'Success Register';
            
            return response($res); 
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed register';

            return response($res);
        }
    }

    public function get_user(Request $request, $id)
    {
        $user = User::where('id', $id)->get();
        if($user){
            $res['success'] = true;
            $res['message'] = $user;
        } else {
            $res['success'] = false;
            $res['message'] = "Can't find user";
        }

        return response($res);
    }

    //
}
